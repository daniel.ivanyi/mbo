package sk.pss.mbo.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import sk.pss.mbo.api.model.Group;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "group")
public class GroupDocument {

    @Id
    private String id;

    private Group data;
}
