package sk.pss.mbo.service;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import sk.pss.mbo.api.model.Group;
import sk.pss.mbo.document.GroupDocument;
import sk.pss.mbo.repository.GroupRepository;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class GroupService {

    private final GroupRepository repository;

    public Group create(Group group) {
        group.setId(ObjectId.get().toHexString());
        var doc = GroupDocument.builder()
                .id(group.getId())
                .data(group)
                .build();
        repository.save(doc);

        return group;
    }

    public List<Group> findAll(String name) {
        List<GroupDocument> groups;
        if (StringUtils.isBlank(name)) {
            groups = repository.findAll();
        } else {
            groups = repository.findAllByName(name);
        }

        return groups.stream().map(GroupDocument::getData).collect(Collectors.toList());
    }

    public Group findOneByUsername(String username) {
        var groups = repository.findOneByUsername(username);
        if (CollectionUtils.isEmpty(groups)) {
            return null;
        }

        return groups.get(0).getData();
    }

    public Group get(String groupId) {
        return findOneOrNotFound(groupId).getData();
    }

    public void remove(String groupId) {
        var doc = findOneOrNotFound(groupId);
        repository.delete(doc);
    }

    public Group update(String groupId, Group group) {
        var doc = findOneOrNotFound(groupId);
        doc.setData(group);
        repository.save(doc);

        return group;
    }

    private GroupDocument findOneOrNotFound(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "group[id=" + id + "] not found"));
    }
}
