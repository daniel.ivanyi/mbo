package sk.pss.mbo.service;

import com.aston.pss.client.email.model.EmailData;
import com.aston.pss.email.rest.EmailApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class NotificationService {

    private final EmailApi emailApi;

    public void sendNotification(String to, String subject,  String text) {
        var data = new EmailData();
        data.setFrom("mbo@pss.sk");
        data.to(to);
        data.setSubject(subject);
        data.setBody(text);

        emailApi.sendMail(data);

       log.info("notification[{}] already send", data);
    }
}
