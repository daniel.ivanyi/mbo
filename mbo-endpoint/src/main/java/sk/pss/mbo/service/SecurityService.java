package sk.pss.mbo.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import sk.pss.mbo.api.model.RoleType;
import sk.pss.shared.endpoint.oauth.model.UserExtended;

import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class SecurityService {

    /**
     * Aktualne prihlaseny pouzivatel.
     *
     * @return login
     */
    public String getLoginUser() {
        return getUser().getUsername();
    }

    /**
     * Vrati usek, na ktorom prihlaseny pouzivatel pracuje.
     *
     * @return usek
     */
    public String getDivision() {
        var user = (UserExtended) getUser();
        var matcher = Pattern.compile("\\((.*)\\)").matcher(user.getFullName());
        if (matcher.find()) {
            return matcher.group(1);
        }

        return null;
    }

    /**
     * Vrati odbor, na ktorom prihlaseny pouzivatel pracuje.
     *
     * @return odbor
     */
    public String getDepartment() {
        var matcher = Pattern.compile("([A-Z]{3})").matcher(getDivision());
        if (matcher.find()) {
            return matcher.group(1);
        }

        return null;
    }

    /**
     * Overenie, ci prihlaseny pouzivatel ma nejaku zo zadanych roli.
     *
     * @param role zoznam roly
     * @return priznak
     */
    public boolean hasAnyRoles(RoleType... role) {
        var roles = Arrays.stream(role).map(Enum::name).collect(Collectors.toList());
        return getUserRoles().stream().anyMatch(a -> roles.contains(a.getAuthority()));
    }

    private User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private Collection<GrantedAuthority> getUserRoles() {
        return getUser().getAuthorities();
    }
}
