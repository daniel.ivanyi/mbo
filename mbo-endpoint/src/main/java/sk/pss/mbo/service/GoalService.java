package sk.pss.mbo.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import sk.pss.mbo.api.model.FindGoalCriteria;
import sk.pss.mbo.api.model.Goal;
import sk.pss.mbo.document.GoalDocument;
import sk.pss.mbo.repository.GoalRepository;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@RequiredArgsConstructor
@Service
public class GoalService {

    private static final String YEAR = "data.year";
    private static final String ASSIGNMENT_TYPE = "data.assignmentType";
    private static final String ASSIGN_TO = "data.assignTo";

    private final MongoTemplate mongoTemplate;
    private final GoalRepository repository;

    public Goal create(Goal goal) {
        goal.setId(ObjectId.get().toHexString());
        var doc = GoalDocument.builder()
                .id(goal.getId())
                .data(goal)
                .build();
        repository.save(doc);

        return goal;
    }

    public List<Goal> findAll(FindGoalCriteria criteria) {
        var query = new Query();
        if (nonNull(criteria.getYear())) {
            query.addCriteria(Criteria.where(YEAR).is(criteria.getYear()));
        }
        if (nonNull(criteria.getAssignmentType())) {
            query.addCriteria(Criteria.where(ASSIGNMENT_TYPE).is(criteria.getAssignmentType()));
        }
        if (nonNull(criteria.getAssignTo())) {
            query.addCriteria(Criteria.where(ASSIGN_TO).in(criteria.getAssignTo()));
        }
        var goals = mongoTemplate.find(query, GoalDocument.class);

        return goals.stream()
                .map(GoalDocument::getData)
                .collect(Collectors.toList());
    }

    public Goal get(String goalId) {
        return findOneOrNotFound(goalId).getData();
    }

    public void remove(String goalId) {
        repository.delete(findOneOrNotFound(goalId));
    }

    public Goal update(String goalId, Goal goal) {
        var doc = findOneOrNotFound(goalId);
        doc.setData(goal);
        repository.save(doc);

        return goal;
    }

    private GoalDocument findOneOrNotFound(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "goal[id=" + id + "] not found"));
    }
}
