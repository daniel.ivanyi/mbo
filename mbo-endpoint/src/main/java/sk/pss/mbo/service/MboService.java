package sk.pss.mbo.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import sk.pss.mbo.api.model.AuditOperation;
import sk.pss.mbo.api.model.FindGoalCriteria;
import sk.pss.mbo.api.model.FindMboCriteria;
import sk.pss.mbo.api.model.GoalAssignmentType;
import sk.pss.mbo.api.model.Lock;
import sk.pss.mbo.api.model.Mbo;
import sk.pss.mbo.document.MboDocument;
import sk.pss.mbo.repository.MboRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@RequiredArgsConstructor
@Service
public class MboService {

    private static final String YEAR = "data.year";
    private static final String USERNAME = "data.username";
    private static final String ASSIGNMENT_TYPE = "data.assignmentType";
    private static final String ASSIGN_TO = "data.assignTo";

    private final MongoTemplate mongoTemplate;
    private final MboRepository repository;

    private final GoalService goalService;
    private final GroupService groupService;
    private final SecurityService securityService;
    private final AuditLogService auditLogService;
    private final NotificationService notificationService;

    public List<Mbo> findMbos(FindMboCriteria criteria) {
        var query = new Query();
        if (nonNull(criteria.getYear())) {
            query.addCriteria(Criteria.where(YEAR).is(criteria.getYear()));
        }
        if (nonNull(criteria.getUsername())) {
            query.addCriteria(Criteria.where(USERNAME).is(criteria.getUsername()));
        }
        if (nonNull(criteria.getAssignmentType())) {
            query.addCriteria(Criteria.where(ASSIGNMENT_TYPE).is(criteria.getAssignmentType()));
        }
        if (nonNull(criteria.getAssignTo())) {
            query.addCriteria(Criteria.where(ASSIGN_TO).in(criteria.getAssignmentType()));
        }
        var mbos = mongoTemplate.find(query, MboDocument.class);

        return mbos.stream()
                .map(MboDocument::getData)
                .collect(Collectors.toList());
    }

    public Mbo createMbo(String username) {
        var year = LocalDate.now().getYear();
        var mbo = new Mbo();
        mbo.setId(ObjectId.get().toHexString());
        mbo.setUsername(username);
        mbo.setYear(year);
        mbo.setGoals(new ArrayList<>());
        // company
        mbo.getGoals().addAll(goalService.findAll(new FindGoalCriteria()
                .year(year)
                .assignmentType(GoalAssignmentType.COMPANY)));
        // deparment
        mbo.getGoals().addAll(goalService.findAll(new FindGoalCriteria()
                .year(year)
                .assignmentType(GoalAssignmentType.DEPARTMENT)
                .assignTo(securityService.getDepartment())));
        // division
        mbo.getGoals().addAll(goalService.findAll(new FindGoalCriteria()
                .year(year)
                .assignmentType(GoalAssignmentType.DEPARTMENT)
                .assignTo(securityService.getDivision())));
        // group
        var userGroup = groupService.findOneByUsername(username);
        if (nonNull(userGroup)) {
            mbo.getGoals().addAll(goalService.findAll(new FindGoalCriteria()
                    .year(year)
                    .assignmentType(GoalAssignmentType.GROUP)
                    .assignTo(userGroup.getName())));
        }
        // person
        mbo.getGoals().addAll(goalService.findAll(new FindGoalCriteria()
                .year(year)
                .assignmentType(GoalAssignmentType.PERSON)
                .assignTo(username)));
        // audit log
        mbo.setAuditLogs(auditLogService.add(AuditOperation.CREATE));
        var doc = MboDocument.builder()
                .id(mbo.getId())
                .data(mbo)
                .build();
        repository.save(doc);

        notificationService.sendNotification("mail@pss.sk", "mbo vytvorene", "mbo vytvorene");

        return mbo;
    }

    public Mbo updateMbo(String mboId, Mbo mbo) {
        mbo.setAuditLogs(auditLogService.add(AuditOperation.UPDATE, mbo.getAuditLogs()));
        var doc = findOneOrNotFound(mboId);
        doc.setData(mbo);
        repository.save(doc);

        if (isNull(mbo.getSignatures().getEmployeeMboSetting())) {
            notificationService.sendNotification("mail@pss.sk",
                    "mbo nastavenie podpis",
                    "mbo nastavenie podpis");
        }

        return mbo;
    }

    public Mbo lock(String mboId) {
        var doc = findOneOrNotFound(mboId);
        var mbo = doc.getData();
        mbo.setLock(new Lock().username(securityService.getLoginUser()).dateOfLock(LocalDateTime.now()));
        mbo.setAuditLogs(auditLogService.add(AuditOperation.LOCK, mbo.getAuditLogs()));
        repository.save(doc);

        return mbo;
    }

    public Mbo unlock(String mboId) {
        var doc = findOneOrNotFound(mboId);
        var mbo = doc.getData();
        mbo.setLock(null);
        mbo.setAuditLogs(auditLogService.add(AuditOperation.LOCK, mbo.getAuditLogs()));
        repository.save(doc);

        return mbo;
    }

    private MboDocument findOneOrNotFound(String id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "mbo[id=" + id + "] not found"));
    }
}
