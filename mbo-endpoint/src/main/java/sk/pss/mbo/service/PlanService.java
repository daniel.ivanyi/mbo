package sk.pss.mbo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.server.ResponseStatusException;
import sk.pss.mbo.api.model.FindGoalCriteria;
import sk.pss.mbo.api.model.Plan;

@RequiredArgsConstructor
@Service
public class PlanService {

    private final GoalService goalService;

    /**
     * Inicializuje plan cielov podla criterii a vrati vygenerovany zoznam.
     *
     * @param criteria vyhladavacie kriteria pre hladanie vhodnych cielov
     * @return plan s cielmi
     */
    public Plan init(FindGoalCriteria criteria) {
        var previousYearCriteria = new FindGoalCriteria()
                .year(criteria.getYear() - 1)
                .assignmentType(criteria.getAssignmentType())
                .assignTo(criteria.getAssignTo());
        var goals = goalService.findAll(previousYearCriteria);
        if (CollectionUtils.isEmpty(goals)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        goals.forEach(g -> {
            g.setYear(criteria.getYear());
            g.setAssignmentType(criteria.getAssignmentType());
            g.setAssignTo(criteria.getAssignTo());
            g.setSemiAnnualEvaluation(null);
            g.setAnnualEvaluation(null);
            g.setTargetReward(null);
            g.setLock(null);

            goalService.create(g);
        });

        return new Plan().year(criteria.getYear())
                .assignmentType(criteria.getAssignmentType())
                .assignTo(criteria.getAssignTo())
                .goals(goals);
    }
}
