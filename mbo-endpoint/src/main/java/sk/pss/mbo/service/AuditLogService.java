package sk.pss.mbo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sk.pss.mbo.api.model.AuditLog;
import sk.pss.mbo.api.model.AuditOperation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
@Service
public class AuditLogService {

    private final SecurityService securityService;

    /**
     * Pridanie audit logu do auditnych logov.
     *
     * @param operation   vykonavana operacia
     * @param description popis
     * @param logs        zoznam logov
     */
    public List<AuditLog> add(AuditOperation operation, String description, List<AuditLog> logs) {
        if (isNull(logs)) {
            logs = new ArrayList<>();
        }
        var log = new AuditLog()
                .date(LocalDateTime.now())
                .username(securityService.getLoginUser())
                .operation(operation)
                .description(description);

        logs.add(log);

        return logs;
    }
    public List<AuditLog> add(AuditOperation operation, List<AuditLog> logs) {
        return add(operation, null, logs);
    }
    public List<AuditLog> add(AuditOperation operation) {
        return add(operation, null, null);
    }
}
