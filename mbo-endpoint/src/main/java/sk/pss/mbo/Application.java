package sk.pss.mbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Spring bootstrap
 */
@SpringBootApplication
@ComponentScan("sk.pss")
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
