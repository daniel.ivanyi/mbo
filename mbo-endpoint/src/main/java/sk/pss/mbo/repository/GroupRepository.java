package sk.pss.mbo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import sk.pss.mbo.document.GroupDocument;

import java.util.List;

public interface GroupRepository extends MongoRepository<GroupDocument, String> {

    /**
     * Vrati skupinu podla mena.
     *
     * @param name nazov skupiny
     * @return najdene skupiny
     */
    @Query("{'data.name' : {'$regex' : ?0}}")
    List<GroupDocument> findAllByName(String name);

    @Query("{'data.persons' : {'$elemMatch' : {'username' : ?0}}}")
    List<GroupDocument> findOneByUsername(String username);
}
