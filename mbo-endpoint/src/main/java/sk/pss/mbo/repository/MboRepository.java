package sk.pss.mbo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import sk.pss.mbo.document.MboDocument;

import java.util.List;

public interface MboRepository extends MongoRepository<MboDocument, String> {

    @Query("{'data.year': ?0, 'data.user': ?1}")
    List<MboDocument> findAllBy(Integer year, String user);
}
