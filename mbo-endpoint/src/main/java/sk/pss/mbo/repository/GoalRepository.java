package sk.pss.mbo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import sk.pss.mbo.document.GoalDocument;

public interface GoalRepository extends MongoRepository<GoalDocument, String> {
}
