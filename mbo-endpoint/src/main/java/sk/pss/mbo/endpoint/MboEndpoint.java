package sk.pss.mbo.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import sk.pss.mbo.api.MboApi;
import sk.pss.mbo.api.model.CreateMboRequest;
import sk.pss.mbo.api.model.FindMboCriteria;
import sk.pss.mbo.api.model.Mbo;
import sk.pss.mbo.service.MboService;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class MboEndpoint implements MboApi {

    private final MboService service;

    @Override
    public ResponseEntity<Mbo> createMbo(CreateMboRequest request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.createMbo(request.getUsername()));
    }

    @Override
    public ResponseEntity<List<Mbo>> findMbos(FindMboCriteria criteria) {
        return ResponseEntity.ok(service.findMbos(criteria));
    }

    @Override
    public ResponseEntity<Mbo> lockMbo(String mboId) {
        return ResponseEntity.ok(service.lock(mboId));
    }

    @Override
    public ResponseEntity<Mbo> unlockMbo(String mboId) {
        return ResponseEntity.ok(service.unlock(mboId));
    }

    @Override
    public ResponseEntity<Mbo> updateMbo(String mboId, Mbo mbo) {
        return ResponseEntity.ok(service.updateMbo(mboId, mbo));
    }
}
