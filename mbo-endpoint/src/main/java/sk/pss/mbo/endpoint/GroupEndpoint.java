package sk.pss.mbo.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import sk.pss.mbo.api.GroupApi;
import sk.pss.mbo.api.model.Group;
import sk.pss.mbo.service.GroupService;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GroupEndpoint implements GroupApi {

    private final GroupService service;

    @Override
    public ResponseEntity<Group> createGroup(Group group) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(group));
    }

    @Override
    public ResponseEntity<List<Group>> findGroups(String name) {
        return ResponseEntity.ok(service.findAll(name));
    }

    @Override
    public ResponseEntity<Group> getGroup(String groupId) {
        return ResponseEntity.ok(service.get(groupId));
    }

    @Override
    public ResponseEntity<Void> removeGroup(String groupId) {
        service.remove(groupId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @Override
    public ResponseEntity<Group> updateGroup(String groupId, Group group) {
        return ResponseEntity.ok(service.update(groupId, group));
    }
}
