package sk.pss.mbo.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import sk.pss.mbo.api.PlanApi;
import sk.pss.mbo.api.model.FindGoalCriteria;
import sk.pss.mbo.api.model.Plan;
import sk.pss.mbo.service.PlanService;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class PlanEndpoint implements PlanApi {

    private final PlanService service;

    @Override
    public ResponseEntity<List<Plan>> findPlans(FindGoalCriteria findPlanCriteria) {
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build();
    }

    @Override
    public ResponseEntity<Plan> initPlan(FindGoalCriteria criteria) {
        return ResponseEntity.ok(service.init(criteria));
    }
}
