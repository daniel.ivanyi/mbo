package sk.pss.mbo.endpoint;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import sk.pss.mbo.api.GoalApi;
import sk.pss.mbo.api.model.FindGoalCriteria;
import sk.pss.mbo.api.model.Goal;
import sk.pss.mbo.service.GoalService;

import java.util.List;

@CrossOrigin
@RestController
@RequiredArgsConstructor
public class GoalEndpoint implements GoalApi {

    private final GoalService service;

    @Override
    public ResponseEntity<Goal> createGoal(Goal goal) {
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(goal));
    }

    @Override
    public ResponseEntity<List<Goal>> findGoals(FindGoalCriteria criteria) {
        return ResponseEntity.ok(service.findAll(criteria));
    }

    @Override
    public ResponseEntity<Goal> getGoal(String goalId) {
        return ResponseEntity.ok(service.get(goalId));
    }

    @Override
    public ResponseEntity<Void> removeGoal(String goalId) {
        service.remove(goalId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
    }

    @Override
    public ResponseEntity<Goal> updateGoal(String goalId, Goal goal) {
        return ResponseEntity.ok(service.update(goalId, goal));
    }
}

