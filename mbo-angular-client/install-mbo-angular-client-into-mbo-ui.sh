#!/usr/bin/env bash

# Set the color variable
green='\033[0;32m'
# Clear the color after that
clear='\033[0m'

swagger_dir="./target/generated-sources/swagger"
mbo_ui="../../../../../../mbo-ui"
mbo_ui_libs="${mbo_ui}/libs"

cd ${swagger_dir}
npm i --force
echo -e  "npm i --force ${green}successful${clear}\r\n"
npm i @types/node@18.11.19 --save-dev --force
echo -e  "npm i @types/node@18.11.19 --save-dev --force ${green}successful${clear}\r\n"
npm run build
echo -e  "npm run build ${green}successful${clear}\r\n"
cd dist
npm pack
echo -e  "npm pack ${green}successful${clear}"
echo -e  "mbo-angular-client as lib ${green}successful${clear}\r\n"
cp mbo-angular-client-1.0.0.tgz ${mbo_ui_libs}
echo -e  "mbo-angular-client copy to ${mbo_ui_libs} ${green}successful${clear}\r\n"
cd ${mbo_ui}
npm i "file://./libs/mbo-angular-client-1.0.0.tgz"
echo -e  "npm i 'file://./libs/mbo-angular-client-1.0.0.tgz' ${green}successful${clear}"
echo -e  "${green}all done${clear}"
